package com.product.innoice.servlets;

import com.google.gson.Gson;
import com.product.innoice.db.InvoicedProductsDataAccessor;
import com.product.innoice.db.InvoicesDataAccessor;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;

public class InvoiceServlet extends HttpServlet {
    @Resource(name = "jdbc/innoice")
    private DataSource dataSource;
    private InvoicesDataAccessor invoicesDataAccessor;
    private InvoicedProductsDataAccessor invoicedProductsDataAccessor;
    private Gson gson = new Gson();

    @Override
    public void init() throws ServletException {
        try {
            invoicesDataAccessor = new InvoicesDataAccessor(dataSource);
            invoicedProductsDataAccessor = new InvoicedProductsDataAccessor(dataSource);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        PrintWriter writer = resp.getWriter();
        final int ID = Integer.parseInt(req.getPathInfo().split("/")[1]);
        writer.println(gson.toJson(invoicesDataAccessor.getInvoiceById(ID)));
        endWriter(writer);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        final int ID = Integer.parseInt(req.getPathInfo().split("/")[1]);
        deleteInvoice(resp, ID);
    }

    private void deleteInvoice(HttpServletResponse resp, final int ID) throws IOException {
        PrintWriter writer = resp.getWriter();
        int response = -1;
        if (invoicedProductsDataAccessor.deleteInvoicedProductsByInvoiceId(ID) > 0) {
            response = invoicesDataAccessor.deleteInvoice(ID);
        }
        writer.println(response);
        endWriter(writer);
    }

    private void endWriter(PrintWriter writer) {
        writer.flush();
        writer.close();
    }

    @Override
    public void destroy() {
        System.out.println("Invoice servlet destroyed");
    }
}
