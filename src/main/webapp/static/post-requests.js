const signUpForm = document.getElementById("signup-form");

signUpForm.addEventListener("submit", e => {
    e.preventDefault();

    const formData = new FormData(signUpForm);

    const password = formData.get('password');
    const confirmPassword = formData.get('confirmPassword');

    if (password !== confirmPassword) {
        alert("Passwords don't match");
        return;
    }

    var encryptedPassword = window.btoa(password);

    const requestData = {
        name: formData.get('name'),
        email: formData.get('email'),
        companyName: formData.get('companyName'),
        address: formData.get('address'),
        contact: formData.get('contact'),
        password: encryptedPassword
    }

    axios.post("http://localhost:8080/InNoice/api/users", requestData)
        .then(({data}) => {
            var message;
            switch (data) {
                case 1:
                    message = "User added successfully";
                    window.location.href = "/InNoice/login";
                    break;
                case -2:
                    message = "An user with this email already exists";
                    break;
                case -3:
                    message = "An user with this mobile number already exists";
                    break;
                default:
                    message = "Something went wrong";            
            }
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});
