<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>InNoice | ${param.pageName}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/ed681c56b9.js" crossorigin="anonymous"></script>
    <!-- <link rel="stylesheet" href="static/styles.css"> -->
    <style>
        * {
            font-family: 'Inter', sans-serif;
            /* background-color: #393E46;
            color: #EEEEEE; */
        }

        /* a {
            color: #A6E3E9;
        } */

        textarea {
            resize: none;
        }

        .row{
            margin: 0;
        }

        .f-400 {
            font-weight: 400;
        }

        .f-500 {
            font-weight: 600;
        }

        .f-600 {
            font-weight: 600;
        }

        .f-700 {
            font-weight: 700;
        }

        .full-height {
            height: 100vh;
        }

        .bg-grad {
            background-image: linear-gradient(to bottom right, #00ADB5, #1F4690);
        }

        .bg-lgray{
            background-color: #EFF5F5;
        }

        .bg-gray {
            background-color: #393E46;
        }

        .text-blue {
            color: #00ADB5;
        }

        .nav-link:hover {
            background-color: black;
        }

        .cursor-pointer {
            cursor: pointer;
        }

        .data-table tr {
            cursor: pointer;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
</head>
<body>
