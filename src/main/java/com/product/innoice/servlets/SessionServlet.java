package com.product.innoice.servlets;

import com.google.gson.Gson;
import com.product.innoice.db.*;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Base64;

public class SessionServlet extends HttpServlet {

    @Resource(name = "jdbc/innoice")
    private DataSource dataSource;
    private SessionsDataAccessor sessionsDataAccessor;
    private Gson gson = new Gson();

    @Override
    public void init() throws ServletException {
        try {
            sessionsDataAccessor = new SessionsDataAccessor(dataSource);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        writer.println("Session servlet");
        endWriter(writer);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        InputStream inputStream = new BufferedInputStream(req.getInputStream());
        String result = IOUtils.toString(inputStream, "UTF-8");
        JSONObject reqObj = new JSONObject(result);
        inputStream.close();
        signInUser(reqObj, req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        HttpSession session = req.getSession();
        session.removeAttribute("userId");
        writer.println(1);
        endWriter(writer);
    }

    private void signInUser(JSONObject reqObj, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();

        final String EMAIL = reqObj.getString("email"),
                PASSWORD = reqObj.getString("password");

        if (EMAIL == null || PASSWORD == null) {
            writer.println("Insufficient data");
            endWriter(writer);
            return;
        }

        byte[] decodedBytes = Base64.getDecoder().decode(PASSWORD);
        final String DECODED_PASSWORD = new String(decodedBytes);

        HttpSession session = req.getSession();
        final int RESULT = sessionsDataAccessor.signInUser(EMAIL, DECODED_PASSWORD, session.getId());

        if (RESULT > 0) {
            session.setAttribute("userId", RESULT);
        }
        writer.println(RESULT);

        endWriter(writer);
    }

    private void endWriter(PrintWriter writer) {
        writer.flush();
        writer.close();
    }

    @Override
    public void destroy() {
        System.out.println("Session servlet destroyed");
    }
}
