package com.product.innoice.servlets;

import com.google.gson.Gson;
import com.product.innoice.db.ProductsDataAccessor;
import com.product.innoice.models.Product;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

public class ProductsServlet extends HttpServlet {
    @Resource(name = "jdbc/innoice")
    private DataSource dataSource;
    private ProductsDataAccessor productsDataAccessor;
    private Gson gson = new Gson();

    @Override
    public void init() throws ServletException {
        try {
            productsDataAccessor = new ProductsDataAccessor(dataSource);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        final String USER_ID_STRING = req.getParameter("userId"),
                response;

        if (USER_ID_STRING == null) {
            response = new JSONObject().put("message", "Insufficient parameters").toString();
        } else if(!USER_ID_STRING.equals(req.getSession().getAttribute("userId").toString())){
            response = new JSONObject().put("message", "No access").toString();
        } else {
            final int USER_ID = Integer.parseInt(USER_ID_STRING);
            response = gson.toJson(productsDataAccessor.getProductsByUser(USER_ID));
        }
        PrintWriter writer = resp.getWriter();
        writer.println(response);
        endWriter(writer);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        InputStream inputStream = new BufferedInputStream(req.getInputStream());
        String result = IOUtils.toString(inputStream, "UTF-8");
        JSONObject reqObj = new JSONObject(result);
        inputStream.close();
        addNewProducts(reqObj, resp);
    }

    private void addNewProducts(JSONObject reqObj, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();

        final String NAME = reqObj.getString("name"),
                UNIT = reqObj.getString("unit");

        final Double PRICE = reqObj.getDouble("price");
        final Integer USER_ID = reqObj.getInt("userId");

        if (NAME == null || UNIT == null || PRICE == null || USER_ID == null) {
            writer.println("Insufficient data");
            endWriter(writer);
            return;
        }

        Product product = new Product(NAME, UNIT, PRICE, USER_ID);
        final int RESPONSE = productsDataAccessor.addNewProduct(product);

        writer.println(RESPONSE);
        endWriter(writer);
    }

    private void endWriter(PrintWriter writer) {
        writer.flush();
        writer.close();
    }

    @Override
    public void destroy() {
        System.out.println("Products servlet destroyed");
    }
}
