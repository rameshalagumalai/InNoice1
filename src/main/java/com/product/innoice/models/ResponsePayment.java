package com.product.innoice.models;

public class ResponsePayment {
    private int id, invoiceId;
    private double amount;
    private String customerName, date;

    public ResponsePayment(int id, int invoiceId, double amount, String customerName, String date) {
        this.id = id;
        this.invoiceId = invoiceId;
        this.amount = amount;
        this.customerName = customerName;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
