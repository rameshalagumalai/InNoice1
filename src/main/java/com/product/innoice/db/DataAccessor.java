package com.product.innoice.db;

import com.product.innoice.models.Customer;
import com.product.innoice.models.Product;
import com.product.innoice.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataAccessor {
    private DataSource dataSource;

    public DataAccessor(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private boolean checkEmail(Connection connection, String table, String email, String extra) throws SQLException {
        final String SQL = "SELECT COUNT(*) AS count FROM " + table + " WHERE email = ?" + extra;
        PreparedStatement preparedStatement = connection.prepareStatement(SQL);
        preparedStatement.setString(1, email);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (!resultSet.next()) {
            preparedStatement.close();
            resultSet.close();
            return false;
        }

        int resRows = resultSet.getInt("count");
        if (resRows != 0) {
            preparedStatement.close();
            resultSet.close();
            return false;
        }

        close(null, preparedStatement, resultSet);
        return true;
    }
    
    private boolean checkMobile(Connection connection, String table, String contact, String extra) throws SQLException {
        final String SQL = "SELECT COUNT(*) AS count FROM " + table + " WHERE contact = ?" + extra;
        PreparedStatement preparedStatement = connection.prepareStatement(SQL);
        preparedStatement.setString(1, contact);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (!resultSet.next()) {
            preparedStatement.close();
            resultSet.close();
            return false;
        }

        int resRows = resultSet.getInt("count");
        if (resRows != 0) {
            preparedStatement.close();
            resultSet.close();
            return false;
        }

        close(null, preparedStatement, resultSet);
        return true;
    }

    private void close(Connection connection, Statement statement, ResultSet resultSet) {
        try {
            if (resultSet != null)  {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connection != null) {
                connection.close();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
