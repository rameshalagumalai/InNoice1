var a = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];

function inWords (num) {
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
    return str;
}

window.onload = () => {
    const href = location.href;
    const id = href.substring(href.lastIndexOf('/')+1);
    axios.get(`http://localhost:8080/InNoice/api/invoices/${id}`)
        .then(({ data }) => {
            displayInvoice(data);
        })
        .catch(e => {
            alert(e.message);
        });
}

function displayInvoice(
    {
        companyName, 
        companyAddress, 
        id, 
        invoiceDate, 
        dueDate,
        customerName,
        customerAddress,
        invoicedProducts,
        total,
        notes,
        termsAndConditions,
        subTotal,
        adjustments,
        shippingCharges,
        balanceDue
    }
) {
    $("#company-name").text(companyName);
    $("#company-address").text(companyAddress);
    $("#id").text(`:INV-${id}`);
    $("#invoice-date").text(`:${invoiceDate}`);
    $("#due-date").text(`:${dueDate}`);
    $("#customer-name").text(customerName);
    $("#customer-address").text(customerAddress);
    const invoicedProductsTable = $("#invoiced-products-table");
    invoicedProducts.map(({ name, quantity, unit, price, discount, total }, i) => {
        invoicedProductsTable.append(`<tr><td>${i+1}</td><td>${name}</td><td><p>${quantity} <span class="text-secondary">${unit}</span></p></td><td>&#8377;${price}</td><td>${discount}%</td><td>&#8377;${total}</td></tr>`)
    });
    $("#total-in-words").text(`Indian Rupee ${inWords(total)} only`);
    $("#notes").text(notes);
    $("#terms-and-conditions").text(termsAndConditions);
    $("#sub-total").text($("#sub-total").text()+subTotal);
    $("#adjustments").text($("#adjustments").text()+adjustments);
    $("#shipping-charges").text($("#shipping-charges").text()+shippingCharges);
    $("#total").text($("#total").text()+total);
    $("#balance-due").text($("#balance-due").text()+balanceDue);
}