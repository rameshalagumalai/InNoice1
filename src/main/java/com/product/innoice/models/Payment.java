package com.product.innoice.models;

public class Payment {

    private int id, invoiceId;
    private double amount;
    private String date;

    public Payment(int invoiceId, double amount, String date) {
        this.invoiceId = invoiceId;
        this.amount = amount;
        this.date = date;
    }

    public Payment(int id, int invoiceId, double amount, String date) {
        this.id = id;
        this.invoiceId = invoiceId;
        this.amount = amount;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
