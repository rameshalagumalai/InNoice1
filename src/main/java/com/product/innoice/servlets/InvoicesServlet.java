package com.product.innoice.servlets;

import com.google.gson.Gson;
import com.product.innoice.db.CustomersDataAccessor;
import com.product.innoice.db.InvoicedProductsDataAccessor;
import com.product.innoice.db.InvoicesDataAccessor;
import com.product.innoice.models.Invoice;
import com.product.innoice.models.InvoicedProduct;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InvoicesServlet extends HttpServlet {
    @Resource(name = "jdbc/innoice")
    private DataSource dataSource;
    private InvoicesDataAccessor invoicesDataAccessor;
    private InvoicedProductsDataAccessor invoicedProductsDataAccessor;
    private CustomersDataAccessor customersDataAccessor;
    private Gson gson = new Gson();

    @Override
    public void init() throws ServletException {
        try {
            invoicesDataAccessor = new InvoicesDataAccessor(dataSource);
            invoicedProductsDataAccessor = new InvoicedProductsDataAccessor(dataSource);
            customersDataAccessor = new CustomersDataAccessor(dataSource);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        final String USER_ID_STRING = req.getParameter("userId"), RESPONSE;
        PrintWriter writer = resp.getWriter();

        if (USER_ID_STRING == null) {
            RESPONSE = new JSONObject().put("message", "Insufficient parameters").toString();
        } else if(!USER_ID_STRING.equals(req.getSession().getAttribute("userId").toString())){
            RESPONSE = new JSONObject().put("message", "No access").toString();
        } else {
            final int USER_ID = Integer.parseInt(USER_ID_STRING);
            try {
                RESPONSE = gson.toJson(invoicesDataAccessor.getInvoicesByUser(USER_ID));
            } catch (SQLException e) {
                throw new ServletException(e);
            }
        }

        writer.println(RESPONSE);
        endWriter(writer);

//        try {
//            getInvoicesByUserId(USER_ID_STRING, resp);
//        } catch (SQLException e) {
//            throw new ServletException(e);
//        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        InputStream inputStream = new BufferedInputStream(req.getInputStream());
        String result = IOUtils.toString(inputStream, "UTF-8");
        JSONObject reqObj = new JSONObject(result);
        inputStream.close();
        addNewInvoice(reqObj, resp);
    }

    private void getInvoicesByUserId(final String USER_ID_STRING, HttpServletResponse resp) throws IOException, SQLException {
        PrintWriter writer = resp.getWriter();

        if (USER_ID_STRING == null) {
            writer.println("{message: 'Insufficient parameters'");
            endWriter(writer);
            return;
        }

        final int USER_ID = Integer.parseInt(USER_ID_STRING);
        writer.println(gson.toJson(invoicesDataAccessor.getInvoicesByUser(USER_ID)));
        endWriter(writer);
    }

    private void addNewInvoice(JSONObject reqObj, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();

        final Integer CUSTOMER_ID = reqObj.getInt("customerId");

        final Double ADJUSTMENTS = reqObj.getDouble("adjustments"),
                SHIPPING_CHARGES = reqObj.getDouble("shippingCharges");

        final String INVOICE_DATE = reqObj.getString("invoiceDate"),
                DUE_DATE = reqObj.getString("dueDate"),
                NOTES = reqObj.getString("notes"),
                TERMS_AND_CONDITIONS = reqObj.getString("termsAndConditions");

        final JSONArray JSON_ARRAY = reqObj.getJSONArray("invoicedProducts");

        List<InvoicedProduct> invoicedProductList = new ArrayList<>();

        final int l = JSON_ARRAY.length();

        Integer productId;
        Double quantity, price, discount, total;
        String name, unit;
        for (int i = 0; i < l; i++) {
            JSONObject jsonObject = (JSONObject) JSON_ARRAY.get(i);
            productId = jsonObject.getInt("id");
            quantity = jsonObject.getDouble("quantity");
            price = jsonObject.getDouble("price");
            discount = jsonObject.getDouble("discount");
            total = jsonObject.getDouble("amount");
            name = jsonObject.getString("name");
            unit = jsonObject.getString("unit");
            if (productId == null || productId == 0 || quantity == null || price == null || discount == null || total == null || name == null || unit == null) {
                continue;
            }
            invoicedProductList.add(new InvoicedProduct(productId, quantity, price, discount, total, name, unit));
        }

        if (invoicedProductList.isEmpty()) {
            writer.println("Insufficient data");
            return;
        }

        if (CUSTOMER_ID == null || ADJUSTMENTS == null || SHIPPING_CHARGES == null || INVOICE_DATE == null || DUE_DATE == null || NOTES == null || TERMS_AND_CONDITIONS == null || JSON_ARRAY == null) {
            writer.println("Insufficient data");
            return;
        }

        Invoice invoice = new Invoice(CUSTOMER_ID, ADJUSTMENTS, SHIPPING_CHARGES, INVOICE_DATE, DUE_DATE, NOTES, TERMS_AND_CONDITIONS);
        final int RESPONSE = invoicesDataAccessor.addNewInvoice(invoice);

        if (RESPONSE == -1) {
            writer.println(-1);
            endWriter(writer);
            return;
        }

        invoicedProductsDataAccessor.addNewInvoicedProducts(invoicedProductList, RESPONSE);

        writer.println(RESPONSE);
        endWriter(writer);
    }

    private void endWriter(PrintWriter writer) {
        writer.flush();
        writer.close();
    }

    @Override
    public void destroy() {
        System.out.println("Invoices servlet destroyed");
    }
}
