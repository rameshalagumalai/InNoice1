alert("Hello");

$("#cancel-new-customer-btn").click(() => {
    history.back();
});

$("#new-customer-form").submit(e => {
    e.preventDefault();

    const formData = new FormData(signUpForm);
    const requestData = {
        name: formData.get('name'),
        email: formData.get('email'),
        address: formData.get('address'),
        contact: formData.get('contact')
    }

    axios.post("http://localhost:8080/InNoice/api/customers?userId=17", requestData)
        .then(({data}) => {
            var message;
            switch (data) {
                case 1:
                    message = "User added successfully";
                    window.location.href = "/InNoice/customers";
                    break;
                case -2:
                    message = "A customer with this email already exists";
                    break;
                case -3:
                    message = "A customer with this mobile number already exists";
                    break;
                default:
                    message = "Something went wrong";            
            }
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});