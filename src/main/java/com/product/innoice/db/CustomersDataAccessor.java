package com.product.innoice.db;

import com.product.innoice.models.Customer;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomersDataAccessor {
    private DataSource dataSource;

    public CustomersDataAccessor(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public int addNewCustomer(Customer customer) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int result = -1;
        try {
            connection = dataSource.getConnection();

            final int USER_ID = customer.getUserId();
            final String EMAIL = customer.getEmail();
            if (!(checkEmail(connection, EMAIL, USER_ID, ""))) {
                return -2;
            }

            final String CONTACT = customer.getContact();
            if (!(checkMobile(connection, CONTACT, USER_ID, ""))) {
                return -3;
            }

            final String NAME = customer.getName(),
                    ADDRESS = customer.getAddress(),
                    SQL = "INSERT INTO customer (name, email, address, contact, user_id) VALUES (?, ?, ?, ?, ?)";

            preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, NAME);
            preparedStatement.setString(2, EMAIL);
            preparedStatement.setString(3, ADDRESS);
            preparedStatement.setString(4, CONTACT);
            preparedStatement.setInt(5, USER_ID);

            result = preparedStatement.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, null);
        }

        return result;
    }

    public int editCustomer(Customer customer) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int result = -1;
        try{
            connection = dataSource.getConnection();

            final int ID = customer.getId(),
                    USER_ID = customer.getUserId();

            final String EXTRA = " AND id != " + ID,
                    EMAIL = customer.getEmail();
            if (!(checkEmail(connection, EMAIL, USER_ID, EXTRA))) {
                return -2;
            }

            final String CONTACT = customer.getContact();
            if (!(checkMobile(connection, EMAIL, USER_ID, EXTRA))) {
                return -3;
            }

            final String NAME = customer.getName(),
                    ADDRESS = customer.getAddress(),
                    SQL = "UPDATE customer SET name = ?, email = ?, address = ?, contact = ? WHERE id = ?";

            preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, NAME);
            preparedStatement.setString(2, EMAIL);
            preparedStatement.setString(3, ADDRESS);
            preparedStatement.setString(4, CONTACT);
            preparedStatement.setInt(5, ID);

            result = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, null);
        }

        return result;
    }

    public List<Customer> getCustomersByUser(int userId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Customer> customerList = new ArrayList<Customer>();

        try {
            connection = dataSource.getConnection();

            final String SQL = "SELECT * FROM customer WHERE user_id = ?";
            preparedStatement = connection.prepareStatement(SQL);

            preparedStatement.setInt(1, userId);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                final int ID = Integer.parseInt(resultSet.getString("id")),
                        USER_ID = Integer.parseInt(resultSet.getString("user_id"));
                final String NAME = resultSet.getString("name"),
                        EMAIL = resultSet.getString("email"),
                        ADDRESS = resultSet.getString("address"),
                        CONTACT = resultSet.getString("contact");
                customerList.add(new Customer(ID, USER_ID, NAME, EMAIL, ADDRESS, CONTACT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }

        return customerList;
    }

    public int deleteCustomer(final int ID) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int result = -1;
        try{
            connection = dataSource.getConnection();

            final String SQL = "DELETE FROM customer WHERE id = ?";

            preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setInt(1, ID);

            result = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, null);
        }

        return result;
    }

    public String getCustomerEmail(final int CUSTOMER_ID) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String email = "";

        try {
            connection = dataSource.getConnection();
            final String SQL = "SELECT email FROM customer WHERE id = ?";
            preparedStatement = connection.prepareStatement(SQL);

            preparedStatement.setInt(1, CUSTOMER_ID);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                email = resultSet.getString("email");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return email;
    }

    private boolean checkEmail(Connection connection, String email, int userId, String extra) throws SQLException {
        final String SQL = "SELECT COUNT(*) AS count FROM customer WHERE user_id = ? AND email = ?" + extra;
        PreparedStatement preparedStatement = connection.prepareStatement(SQL);
        preparedStatement.setInt(1, userId);
        preparedStatement.setString(2, email);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (!resultSet.next()) {
            preparedStatement.close();
            resultSet.close();
            return false;
        }

        int resRows = resultSet.getInt("count");
        if (resRows != 0) {
            preparedStatement.close();
            resultSet.close();
            return false;
        }

        close(null, preparedStatement, resultSet);
        return true;
    }

    private boolean checkMobile(Connection connection, String contact, int userId, String extra) throws SQLException {
        final String SQL = "SELECT COUNT(*) AS count FROM customer WHERE user_id = ? AND contact = ?" + extra;
        PreparedStatement preparedStatement = connection.prepareStatement(SQL);
        preparedStatement.setInt(1, userId);
        preparedStatement.setString(2, contact);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (!resultSet.next()) {
            preparedStatement.close();
            resultSet.close();
            return false;
        }

        int resRows = resultSet.getInt("count");
        if (resRows != 0) {
            preparedStatement.close();
            resultSet.close();
            return false;
        }

        close(null, preparedStatement, resultSet);
        return true;
    }

    private void close(Connection connection, Statement statement, ResultSet resultSet) {
        try {
            if (resultSet != null)  {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connection != null) {
                connection.close();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
