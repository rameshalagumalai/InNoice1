package com.product.innoice.models;

public class InvoicedProduct {
    private int id, invoiceId, productId;
    private double quantity, price, discount, total;
    private String name, unit;

    public InvoicedProduct(int productId, double quantity, double price, double discount, double total, String name, String unit) {
        this.productId = productId;
        this.quantity = quantity;
        this.price = price;
        this.discount = discount;
        this.total = total;
        this.name = name;
        this.unit = unit;
    }

    public InvoicedProduct(int id, int invoiceId, int productId, double quantity, double price, double discount, double total, String name, String unit) {
        this.id = id;
        this.invoiceId = invoiceId;
        this.productId = productId;
        this.quantity = quantity;
        this.price = price;
        this.discount = discount;
        this.total = total;
        this.name = name;
        this.unit = unit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
