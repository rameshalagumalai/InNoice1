const loginForm = document.getElementById("login-form");

loginForm.addEventListener("submit", e => {
    e.preventDefault();

    const formData = new FormData(loginForm);
    const requestData = {
        email: formData.get('email'),
        password: window.btoa(formData.get('password'))
    }

    axios.post("http://localhost:8080/InNoice/api/sessions", requestData)
        .then(({ data }) => {
            var message;
            switch (data) {
                case -1:
                    message = "Something went wrong";
                    break;
                case -2:
                    message = "Your email or password may be wrong, try again";
                    break;
                default:
                    location.reload();    
            }
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});