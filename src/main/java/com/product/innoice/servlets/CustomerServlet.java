package com.product.innoice.servlets;

import com.google.gson.Gson;
import com.product.innoice.db.CustomersDataAccessor;
import com.product.innoice.models.Customer;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

public class CustomerServlet extends HttpServlet {
    @Resource(name = "jdbc/innoice")
    private DataSource dataSource;
    private CustomersDataAccessor customersDataAccessor;
    private Gson gson = new Gson();

    @Override
    public void init() throws ServletException {
        try {
            customersDataAccessor = new CustomersDataAccessor(dataSource);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        String id = req.getPathInfo().split("/")[1];
        writer.println(id);
        endWriter(writer);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        InputStream inputStream = new BufferedInputStream(req.getInputStream());
        String result = IOUtils.toString(inputStream, "UTF-8");
        String id = req.getPathInfo().split("/")[1];
        JSONObject reqObj = new JSONObject(result);
        reqObj.put("id", id);
        inputStream.close();
        editCustomer(reqObj, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        final int ID = Integer.parseInt(req.getPathInfo().split("/")[1]);
        final int RESPONSE = customersDataAccessor.deleteCustomer(ID);
        writer.println(RESPONSE);
        endWriter(writer);
    }

    private void editCustomer(JSONObject reqObj, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();

        final String NAME = reqObj.getString("name"),
                EMAIL = reqObj.getString("email"),
                ADDRESS = reqObj.getString("address"),
                CONTACT = reqObj.getString("contact");
        final Integer ID = reqObj.getInt("id"),
                USER_ID = reqObj.getInt("userId");

        if (NAME == null || EMAIL == null || ADDRESS == null || CONTACT == null || ID == null || USER_ID == null) {
            writer.println("Insufficient data");
            return;
        }

        Customer customer = new Customer(ID, USER_ID, NAME, EMAIL, ADDRESS, CONTACT);
        final int RESPONSE = customersDataAccessor.editCustomer(customer);

        writer.println(RESPONSE);
        endWriter(writer);
    }

    private void endWriter(PrintWriter writer) {
        writer.flush();
        writer.close();
    }

    @Override
    public void destroy() {
        System.out.println("Customer servlet destroyed");
    }
}
