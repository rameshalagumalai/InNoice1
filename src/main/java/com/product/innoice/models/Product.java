package com.product.innoice.models;

public class Product {
    private int id, userId;
    private String name, unit;
    private double price, totalSales;

    public Product(String name, String unit, double price, int userId) {
        this.userId = userId;
        this.name = name;
        this.unit = unit;
        this.price = price;
    }

    public Product(int id, String name, String unit, double price, double totalSales, int userId) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.unit = unit;
        this.price = price;
        this.totalSales = totalSales;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(double totalSales) {
        this.totalSales = totalSales;
    }
}
