$("#logout-btn").click(() => {
    axios.delete("http://localhost:8080/InNoice/api/sessions")
        .then(({ data }) => {
            var message;
            if (data === 1) {
                message = "Successfully logged out";
                location.reload();
            } else {
                message = "Something went wrong";
            }
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});