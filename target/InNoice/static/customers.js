var customers = [];

window.onload = () => {
    axios.get("http://localhost:8080/InNoice/api/customers?userId=19")
        .then(({ data }) => {
            customers = data;
            const customersTable = $("#customers-table");
            customers.map(({ id, name, email, contact }, i) => {
                customersTable.append(`<tr><td>${name}</td><td><a class="text-decoration-none" href="mailto:${email}">${email}</a></td><td>${contact}</td><td>0</td><td>0</td><td><button class="btn btn-outline-secondary btn-sm px-2 py-1 me-1" data-bs-toggle="modal" data-bs-target="#exampleModal1" onclick="setEditModalContent(${i})">Edit</button><button class="btn btn-outline-danger btn-sm px-2 py-1" data-bs-toggle="modal" data-bs-target="#exampleModal4" onclick="$('#delete-id').val(${id})">Delete</button></td></tr>`);
            });
        })
        .catch(e => {
            alert(e.message);
        });
}

const newCustomerForm = document.getElementById("new-customer-form");

newCustomerForm.addEventListener("submit", e => {
    e.preventDefault();

    const formData = new FormData(newCustomerForm);
    const requestData = {
        name: formData.get('name'),
        email: formData.get('email'),
        address: formData.get('address'),
        contact: formData.get('contact'),
        userId: 19
    }

    axios.post("http://localhost:8080/InNoice/api/customers", requestData)
        .then(({ data }) => {
            var message;
            switch (data) {
                case 1:
                    message = "User added successfully";
                    location.reload();
                    break;
                case -2:
                    message = "A customer with this email already exists";
                    break;
                case -3:
                    message = "A customer with this mobile number already exists";
                    break;
                default:
                    message = "Something went wrong";
            }
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});

function setEditModalContent(i) {
    const { id, userId, name, email, address, contact } = customers[i];
    $("#edit-id").val(id);
    $("#edit-user-id").val(userId)
    $("#edit-name").val(name);
    $("#edit-email").val(email);
    $("#edit-address").val(address);
    $("#edit-contact").val(contact);
}

const editCustomerForm = document.getElementById("edit-customer-form");

editCustomerForm.addEventListener("submit", e => {
    e.preventDefault();

    const formData = new FormData(editCustomerForm);

    const requestData = {
        userId: formData.get('userId'),
        name: formData.get('name'),
        email: formData.get('email'),
        address: formData.get('address'),
        contact: formData.get('contact'),
    }

    axios.put(`http://localhost:8080/InNoice/api/customers/${formData.get('id')}`, requestData)
        .then(({ data }) => {
            var message;
            switch (data) {
                case 1:
                    message = "Changes made to the customer successfully";
                    location.reload();
                    break;
                case -2:
                    message = "A customer with this email already exists";
                    break;
                case -3:
                    message = "Something went wrong";
            }        
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});

const deleteCustomerForm = document.getElementById("delete-customer-form");

deleteCustomerForm.addEventListener("submit", e => {
    e.preventDefault();

    const formData = new FormData(deleteCustomerForm);

    axios.delete(`http://localhost:8080/InNoice/api/customers/${formData.get('id')}`)
        .then(({ data }) => {
            var message;
            if (data === 1) {
                message = "Customer deleted successfully";
                location.reload();
            } else {
                message = "Something went wrong";
            }   
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});

// $("#new-customer-page-btn").click(() => {
//     window.location.href = "/InNoice/customers/new";
// });