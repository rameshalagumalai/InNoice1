package com.product.innoice.servlets;

import com.google.gson.Gson;
import com.product.innoice.db.PaymentsDataAccessor;
import com.product.innoice.models.Payment;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

public class PaymentsServlet extends HttpServlet {
    @Resource(name = "jdbc/innoice")
    private DataSource dataSource;
    private PaymentsDataAccessor paymentsDataAccessor;
    private Gson gson = new Gson();

    @Override
    public void init() throws ServletException {
        try {
            paymentsDataAccessor = new PaymentsDataAccessor(dataSource);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        final String USER_ID_STRING = req.getParameter("userId"),
                response;
        if (USER_ID_STRING != null) {
            final int USER_ID = Integer.parseInt(USER_ID_STRING);
            response = gson.toJson(paymentsDataAccessor.getPaymentsByUser(USER_ID));
        } else {
            response = "{message: 'Insufficient parameters'}";
        }
        PrintWriter writer = resp.getWriter();
        writer.println(response);
        endWriter(writer);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        InputStream inputStream = new BufferedInputStream(req.getInputStream());
        String result = IOUtils.toString(inputStream, "UTF-8");
        JSONObject reqObj = new JSONObject(result);
        inputStream.close();
        addNewPayment(reqObj, resp);
    }

    private void addNewPayment(JSONObject reqObj, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();

        final Integer INVOICE_ID = reqObj.getInt("invoiceId");
        final Double AMOUNT = reqObj.getDouble("amount");
        final String DATE = reqObj.getString("date");

        if (INVOICE_ID == null || AMOUNT == null || DATE == null) {
            writer.println("Insufficient data");
            return;
        }

        Payment payment = new Payment(INVOICE_ID, AMOUNT, DATE);
        writer.println(paymentsDataAccessor.addNewPayment(payment));
        endWriter(writer);
    }

    private void endWriter(PrintWriter writer) {
        writer.flush();
        writer.close();
    }

    @Override
    public void destroy() {
        System.out.println("Payments servlet destroyed");
    }
}
