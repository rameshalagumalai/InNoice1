package com.product.innoice.db;

import com.product.innoice.models.Product;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ProductsDataAccessor {
    private DataSource dataSource;

    public ProductsDataAccessor(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Product> getProductsByUser(int userId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Product> productList = new ArrayList<Product>();

        try {
            connection = dataSource.getConnection();

            final String SQL = "SELECT product.*, (SELECT SUM(total) FROM invoiced_product WHERE product_id = product.id) AS total_sales FROM product WHERE user_id = ?";
            preparedStatement = connection.prepareStatement(SQL);

            preparedStatement.setInt(1, userId);
            resultSet = preparedStatement.executeQuery();

            int id, user_id;
            String name, unit;
            double price, totalSales;
            while (resultSet.next()) {
                id = resultSet.getInt("id");
                user_id = resultSet.getInt("user_id");
                name = resultSet.getString("name");
                unit = resultSet.getString("unit");
                price = resultSet.getDouble("price");
                totalSales = resultSet.getDouble("total_sales");
                productList.add(new Product(id, name, unit, price, totalSales, user_id));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }

        return productList;
    }

    public int addNewProduct(Product product) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int result = -1;
        try{
            connection = dataSource.getConnection();

            final int USER_ID = product.getUserId();
            final double PRICE = product.getPrice();
            final String NAME = product.getName(),
                    UNIT = product.getUnit(),
                    SQL = "INSERT INTO product (name, unit, price, user_id) VALUES (?, ?, ?, ?)";

            preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, NAME);
            preparedStatement.setString(2, UNIT);
            preparedStatement.setDouble(3, PRICE);
            preparedStatement.setInt(4, USER_ID);

            result = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, null);
        }

        return result;
    }

    public int editProduct(Product product) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int result = -1;
        try{
            connection = dataSource.getConnection();

            final int ID = product.getId(),
                    USER_ID = product.getUserId();
            final double PRICE = product.getPrice();
            final String NAME = product.getName(),
                    UNIT = product.getUnit(),
                    SQL = "UPDATE product SET name = ?, unit = ?, price = ? WHERE id = ?";

            preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, NAME);
            preparedStatement.setString(2, UNIT);
            preparedStatement.setDouble(3, PRICE);
            preparedStatement.setInt(4, ID);

            result = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, null);
        }

        return result;
    }

    public int deleteProduct(final int ID) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int result = -1;
        try{
            connection = dataSource.getConnection();

            final String SQL = "DELETE FROM product WHERE id = ?";

            preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setInt(1, ID);

            result = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, null);
        }

        return result;
    }

    private void close(Connection connection, Statement statement, ResultSet resultSet) {
        try {
            if (resultSet != null)  {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connection != null) {
                connection.close();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
