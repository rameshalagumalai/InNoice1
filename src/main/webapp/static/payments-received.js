var payments = [];
const userId = $("#user-id").val();

window.onload = () => {
    axios.get(`http://localhost:8080/InNoice/api/payments?userId=${userId}`)
        .then(({ data }) => {
            payments = data;
            const paymentsTable = $("#payments-table");
            payments.map(({ id, invoiceId, amount, customerName, date }, i) => {
                paymentsTable.append(`<tr><td onclick="redirectToPaymentPage(${id})">${date}</td><td onclick="redirectToPaymentPage(${id})">PAY-${id}</td><td onclick="redirectToPaymentPage(${id})">INV-${invoiceId}</td><td onclick="redirectToPaymentPage(${id})">${customerName}</td><td onclick="redirectToPaymentPage(${id})">&#8377;${amount}</td><td><button class="btn btn-outline-secondary btn-sm px-2 py-1 me-1" data-bs-toggle="modal" data-bs-target="#exampleModal9" onclick="setEditModalContent(${i})">Edit</button><button class="btn btn-outline-danger btn-sm px-2 py-1" data-bs-toggle="modal" data-bs-target="#exampleModal10" onclick="$('#delete-id').val(${id})">Delete</button></td></tr>`);
            });
        })
        .catch(e => {
            alert(e.message);
        });
}

function redirectToPaymentPage(id) {
    location.href = `/InNoice/payments-received/${id}`;
}

function setEditModalContent(i) {
    const { id, invoiceId, date, amount } = payments[i];

    $("#edit-id").val(id);
    $("#paid-invoice-id").val(invoiceId);
    $("#payment-date").val(date);
    $("#payment-amount").val(amount);
}

const editPaymentForm = document.getElementById("edit-payment-form");

editPaymentForm.addEventListener("submit", e => {
    e.preventDefault();

    const formData = new FormData(editPaymentForm);
    const requestData = {
        invoiceId: formData.get("invoice-id"),
        date: formData.get("payment-date"),
        amount: formData.get("payment-amount")
    }

    console.log(formData.get("id"));

    axios.put(`http://localhost:8080/InNoice/api/payments/${formData.get("id")}`, requestData)
        .then(({ data }) => {
            var message;
            switch (data) {
                case 1:
                    message = "Payment edited successfully";
                    location.reload();
                    break;
                case -2:
                    message = "Payment exceeds the balance due";
                    break;
                case -3:
                    message = "Invalid payment amount";
                    break;
                case -4:
                    message = "Payment must be done after the issue of the invoice";
                    break;    
                default:
                    message = "Something went wrong";
            }
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});

const deletePaymentForm = document.getElementById("delete-payment-form");

deletePaymentForm.addEventListener("submit", e => {
    e.preventDefault();

    const formData = new FormData(deletePaymentForm);

    axios.delete(`http://localhost:8080/InNoice/api/payments/${formData.get('id')}`)
        .then(({ data }) => {
            var message;
            if (data === 1) {
                message = "Payment deleted successfully";
                location.reload();
            } else {
                message = "Something went wrong";
            }   
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});
