package com.product.innoice.db;

import com.product.innoice.models.User;
import org.jasypt.util.text.BasicTextEncryptor;

import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.sql.*;

public class UsersDataAccessor {
    private DataSource dataSource;

    public UsersDataAccessor(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public int addNewUser(User user, HttpSession session) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int result = -1;
        try {
            connection = dataSource.getConnection();

            String EMAIL = user.getEmail();
            if (!(checkEmail(connection, "user", EMAIL, ""))) {
                return -2;
            }

            String CONTACT = user.getContact();
            if (!(checkMobile(connection, "user", CONTACT, ""))) {
                return -3;
            }

            BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
            textEncryptor.setPasswordCharArray("yuvaraaj".toCharArray());

            final String NAME = user.getName(),
                    COMPANY_NAME = user.getCompanyName(),
                    ADDRESS = user.getAddress(),
                    PASSWORD = textEncryptor.encrypt(user.getPassword()),
                    SQL = "INSERT INTO user (name, email, company_name, address, contact, password) VALUES (?, ?, ?, ?, ?, ?)";

            preparedStatement = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, NAME);
            preparedStatement.setString(2, EMAIL);
            preparedStatement.setString(3, COMPANY_NAME);
            preparedStatement.setString(4,  ADDRESS);
            preparedStatement.setString(5, CONTACT);
            preparedStatement.setString(6, PASSWORD);

            result = preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                session.setAttribute("userId", resultSet.getInt(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, null);
        }

        return result;
    }

    private boolean checkEmail(Connection connection, String table, String email, String extra) throws SQLException {
        final String SQL = "SELECT COUNT(*) AS count FROM " + table + " WHERE email = ?" + extra;
        PreparedStatement preparedStatement = connection.prepareStatement(SQL);
        preparedStatement.setString(1, email);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (!resultSet.next()) {
            preparedStatement.close();
            resultSet.close();
            return false;
        }

        int resRows = resultSet.getInt("count");
        if (resRows != 0) {
            preparedStatement.close();
            resultSet.close();
            return false;
        }

        close(null, preparedStatement, resultSet);
        return true;
    }

    private boolean checkMobile(Connection connection, String table, String contact, String extra) throws SQLException {
        final String SQL = "SELECT COUNT(*) AS count FROM " + table + " WHERE contact = ?" + extra;
        PreparedStatement preparedStatement = connection.prepareStatement(SQL);
        preparedStatement.setString(1, contact);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (!resultSet.next()) {
            preparedStatement.close();
            resultSet.close();
            return false;
        }

        int resRows = resultSet.getInt("count");
        if (resRows != 0) {
            preparedStatement.close();
            resultSet.close();
            return false;
        }

        close(null, preparedStatement, resultSet);
        return true;
    }

    private void close(Connection connection, Statement statement, ResultSet resultSet) {
        try {
            if (resultSet != null)  {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connection != null) {
                connection.close();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
