package com.product.innoice.models;

import java.util.List;

public class ResponseInvoice {
    private int id;
    private double adjustments, shippingCharges, subTotal, total, amountPaid, balanceDue;
    private String customerName, customerAddress, companyName, companyAddress, invoiceDate, dueDate, notes, termsAndConditions;
    private List<InvoicedProduct> invoicedProducts;

    public ResponseInvoice(int id, double adjustments, double shippingCharges, double subTotal, double total, double amountPaid, double balanceDue, String customerName, String customerAddress, String companyName, String companyAddress, String invoiceDate, String dueDate, String notes, String termsAndConditions, List<InvoicedProduct> invoicedProducts) {
        this.id = id;
        this.adjustments = adjustments;
        this.shippingCharges = shippingCharges;
        this.subTotal = subTotal;
        this.total = total;
        this.amountPaid = amountPaid;
        this.balanceDue = balanceDue;
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.companyName = companyName;
        this.companyAddress = companyAddress;
        this.invoiceDate = invoiceDate;
        this.dueDate = dueDate;
        this.notes = notes;
        this.termsAndConditions = termsAndConditions;
        this.invoicedProducts = invoicedProducts;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAdjustments() {
        return adjustments;
    }

    public void setAdjustments(double adjustments) {
        this.adjustments = adjustments;
    }

    public double getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(double shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }

    public double getBalanceDue() {
        return balanceDue;
    }

    public void setBalanceDue(double balanceDue) {
        this.balanceDue = balanceDue;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public List<InvoicedProduct> getInvoicedProducts() {
        return invoicedProducts;
    }

    public void setInvoicedProducts(List<InvoicedProduct> invoicedProducts) {
        this.invoicedProducts = invoicedProducts;
    }
}
