package com.product.innoice.servlets;

import com.google.gson.Gson;
import com.product.innoice.db.ProductsDataAccessor;
import com.product.innoice.models.Product;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

public class ProductServlet extends HttpServlet {
    @Resource(name = "jdbc/innoice")
    private DataSource dataSource;
    private ProductsDataAccessor productsDataAccessor;
    private Gson gson = new Gson();

    @Override
    public void init() throws ServletException {
        try {
            productsDataAccessor = new ProductsDataAccessor(dataSource);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        String id = req.getPathInfo().split("/")[1];
        writer.println(id);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        InputStream inputStream = new BufferedInputStream(req.getInputStream());
        String result = IOUtils.toString(inputStream, "UTF-8");
        String id = req.getPathInfo().split("/")[1];
        JSONObject reqObj = new JSONObject(result);
        reqObj.put("id", id);
        inputStream.close();
        editProduct(reqObj, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        final int ID = Integer.parseInt(req.getPathInfo().split("/")[1]);
        final int RESPONSE = productsDataAccessor.deleteProduct(ID);
        writer.println(RESPONSE);
        endWriter(writer);
    }

    private void editProduct(JSONObject reqObj, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();

        final String NAME = reqObj.getString("name"),
                UNIT = reqObj.getString("unit");
        final Double PRICE = reqObj.getDouble("price");
        final Integer ID = reqObj.getInt("id"),
                USER_ID = reqObj.getInt("userId");

        if (NAME == null || UNIT == null || PRICE == null || ID == null || USER_ID == null) {
            writer.println("Insufficient data");
            return;
        }

        Product product = new Product(ID, NAME, UNIT, PRICE, 0, USER_ID);
        final int RESPONSE = productsDataAccessor.editProduct(product);

        writer.println(RESPONSE);
        endWriter(writer);
    }

    private void endWriter(PrintWriter writer) {
        writer.flush();
        writer.close();
    }

    @Override
    public void destroy() {
        System.out.println("User servlet destroyed");
    }
}
