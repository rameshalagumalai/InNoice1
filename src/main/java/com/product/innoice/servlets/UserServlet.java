package com.product.innoice.servlets;

import com.product.innoice.db.UsersDataAccessor;
import com.product.innoice.models.User;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.*;
import java.util.Base64;

public class UserServlet extends HttpServlet {
    @Resource(name = "jdbc/innoice")
    private DataSource dataSource;
    private UsersDataAccessor usersDataAccessor;

    @Override
    public void init() throws ServletException {
        try {
            usersDataAccessor = new UsersDataAccessor(dataSource);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        writer.println("Hello");
        endWriter(writer);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        InputStream inputStream = new BufferedInputStream(req.getInputStream());
        String result = IOUtils.toString(inputStream, "UTF-8");
        JSONObject reqObj = new JSONObject(result);
        inputStream.close();
        addNewUser(reqObj, req, resp);
    }

    private void addNewUser(JSONObject reqObj, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();

        final String NAME = reqObj.getString("name"),
                     EMAIL = reqObj.getString("email"),
                     COMPANY_NAME = reqObj.getString("companyName"),
                     ADDRESS = reqObj.getString("address"),
                     CONTACT = reqObj.getString("contact"),
                     PASSWORD = reqObj.getString("password");

        if (NAME == null || EMAIL == null || COMPANY_NAME == null || ADDRESS == null || CONTACT == null || PASSWORD == null || NAME.isEmpty() || EMAIL.isEmpty() || COMPANY_NAME.isEmpty() || ADDRESS.isEmpty() || CONTACT.isEmpty() || PASSWORD.isEmpty()) {
            writer.println("Insufficient data");
            return;
        }

        byte[] decodedBytes = Base64.getDecoder().decode(PASSWORD);
        final String DECODED_PASSWORD = new String(decodedBytes);

        HttpSession session = req.getSession();
        User user = new User(NAME, EMAIL, COMPANY_NAME, ADDRESS, CONTACT, DECODED_PASSWORD);
        final int RESPONSE = usersDataAccessor.addNewUser(user, session);

        writer.println(RESPONSE);
        endWriter(writer);
    }

    private void endWriter(PrintWriter writer) {
        writer.flush();
        writer.close();
    }

    @Override
    public void destroy() {
        System.out.println("User servlet destroyed");
    }
}
