var products = [];
const userId = $("#user-id").val();

window.onload = () => {
    axios.get(`http://localhost:8080/InNoice/api/products?userId=${userId}`)
        .then(({ data }) => {
            products = data;
            const productsTable = $("#products-table");
            products.map(({ id, name, unit, price, totalSales }, i) => {
                productsTable.append(`<tr><td>${name}</td><td>${unit}</td><td>&#8377;${price}</td><td>&#8377;${totalSales}</td><td><button class="btn btn-outline-secondary btn-sm px-2 py-1 me-1" data-bs-toggle="modal" data-bs-target="#exampleModal3" onclick="setEditModalContent(${i})">Edit</button><button class="btn btn-outline-danger btn-sm px-2 py-1" data-bs-toggle="modal" data-bs-target="#exampleModal5" onclick="$('#delete-id').val(${id})">Delete</button></td></tr>`);
            });
        })
        .catch(e => {
            alert(e.message);
        });
}

const newProductForm = document.getElementById("new-product-form");

newProductForm.addEventListener("submit", e => {
    e.preventDefault();

    const formData = new FormData(newProductForm);
    const requestData = {
        name: formData.get('name'),
        unit: formData.get('unit'),
        price: formData.get('price'),
        userId: userId
    }

    axios.post("http://localhost:8080/InNoice/api/products", requestData)
        .then(({ data }) => {
            var message;
            if (data === 1) {
                message = "Product added successfully";
                location.reload();
            } else {
                message = "Something went wrong";
            }
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});

function setEditModalContent(i) {
    const { id, userId, name, unit, price } = products[i];
    $("#edit-id").val(id);
    $("#edit-user-id").val(userId)
    $("#edit-name").val(name);
    $("#edit-unit").val(unit);
    $("#edit-price").val(price);
}

const editProductForm = document.getElementById("edit-product-form");

editProductForm.addEventListener("submit", e => {
    e.preventDefault();

    const formData = new FormData(editProductForm);

    const requestData = {
        userId: formData.get('userId'),
        name: formData.get('name'),
        unit: formData.get('unit'),
        price: formData.get('price'),
    }

    axios.put(`http://localhost:8080/InNoice/api/products/${formData.get('id')}`, requestData)
        .then(({ data }) => {
            var message;
            if (data === 1) {
                message = "Changes made to the product successfully";
                location.reload();
            } else {
                message = "Something went wrong";
            }
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});

const deleteProductForm = document.getElementById("delete-product-form");

deleteProductForm.addEventListener("submit", e => {
    e.preventDefault();

    const formData = new FormData(deleteProductForm);

    axios.delete(`http://localhost:8080/InNoice/api/products/${formData.get('id')}`)
        .then(({ data }) => {
            var message;
            if (data === 1) {
                message = "Product deleted successfully";
                location.reload();
            } else {
                message = "Something went wrong";
            }      
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});

// $("#new-product-page-btn").click(() => {
//     window.location.href = "/InNoice/products/new";
// });