package com.product.innoice.db;

import com.product.innoice.models.InvoicedProduct;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class InvoicedProductsDataAccessor {
    private DataSource dataSource;

    public InvoicedProductsDataAccessor(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public int addNewInvoicedProducts(List<InvoicedProduct> invoicedProducts, final int INVOICE_ID) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int response = -1;

        try {
            connection = dataSource.getConnection();

            int productId, result;
            double quantity, price, discount, total;
            String name, unit;
            final String SQL = "INSERT INTO invoiced_product (invoice_id, product_id, name, quantity, price, discount, total, unit) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

            for(InvoicedProduct invoicedProduct : invoicedProducts) {
                productId = invoicedProduct.getProductId();
                quantity = invoicedProduct.getQuantity();
                price = invoicedProduct.getPrice();
                discount = invoicedProduct.getDiscount();
                total = invoicedProduct.getTotal();
                name = invoicedProduct.getName();
                unit = invoicedProduct.getUnit();
                preparedStatement = connection.prepareStatement(SQL);
                preparedStatement.setInt(1, INVOICE_ID);
                preparedStatement.setInt(2, productId);
                preparedStatement.setString(3, name);
                preparedStatement.setDouble(4, quantity);
                preparedStatement.setDouble(5, price);
                preparedStatement.setDouble(6, discount);
                preparedStatement.setDouble(7, total);
                preparedStatement.setString(8, unit);
                preparedStatement.execute();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, null);
        }

        return response;
    }

    public List<InvoicedProduct> getInvoicedProductsByInvoice(int invoiceId) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<InvoicedProduct> invoicedProducts = new ArrayList<>();

        try {
            connection = dataSource.getConnection();
            final String SQL = "SELECT * FROM invoiced_product WHERE invoice_id = ?";
            preparedStatement = connection.prepareStatement(SQL);

            preparedStatement.setInt(1, invoiceId);
            resultSet = preparedStatement.executeQuery();

            int id, invoice_id, productId;
            String name, unit;
            double quantity, price, discount, total;
            while (resultSet.next()) {
                id = resultSet.getInt("id");
                invoice_id = resultSet.getInt("invoice_id");
                productId = resultSet.getInt("product_id");
                name = resultSet.getString("name");
                quantity = resultSet.getDouble("quantity");
                price = resultSet.getDouble("price");
                discount = resultSet.getDouble("discount");
                total = resultSet.getDouble("total");
                unit = resultSet.getString("unit");
                invoicedProducts.add(new InvoicedProduct(id, invoice_id, productId, quantity, price, discount, total, name, unit));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }

        return invoicedProducts;
    }

    public int deleteInvoicedProductsByInvoiceId(final int INVOICE_ID) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int totalPayments = -1;
        int result = -1;
        try{
            connection = dataSource.getConnection();

            final String PSQL = "SELECT COUNT(*) AS payments FROM payment WHERE invoice_id = ?";
            preparedStatement = connection.prepareStatement(PSQL);

            preparedStatement.setInt(1, INVOICE_ID);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                totalPayments = resultSet.getInt("payments");
            }

            if(totalPayments != -1 || totalPayments == 0) {
                final String SQL = "DELETE FROM invoiced_product WHERE invoice_id = ?";

                preparedStatement = connection.prepareStatement(SQL);
                preparedStatement.setInt(1, INVOICE_ID);

                result = preparedStatement.executeUpdate();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, null);
        }

        return result;
    }

    private void close(Connection connection, Statement statement, ResultSet resultSet) {
        try {
            if (resultSet != null)  {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connection != null) {
                connection.close();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
