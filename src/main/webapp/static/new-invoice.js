var customers = [];
var products = [];
var selectedProductsCount = 1;
var subTotal = 0;
var total = 0;
var adjustments = 0;
var shippingCharges = 0;
const userId = $("#user-id").val();

// $("#cancel-new-invoice-btn").click(() => {
//     history.back();
// });

const productsTable = $("#products-table");

var selectedProducts = [
    {
        id: 0,
        name: "",
        quantity: 1,
        unit: "",
        price: 0,
        discount: 0,
        amount: 0
    }
]

window.onload = () => {
    axios.get(`http://localhost:8080/InNoice/api/customers?userId=${userId}`)
        .then(({ data }) => {
            customers = data;
            const selectCustomer = $("#select-customer");
            customers.map(({ id, name }, i) => {
                selectCustomer.append(`<option value=${id}>${name}</option>`);
            });
        })
        .catch(e => {
            alert(e.message);
        });

    axios.get(`http://localhost:8080/InNoice/api/products?userId=${userId}`)
        .then(({ data }) => {
            products = data;
            const selectProduct= $("#select-product-0");
            products.map(({ name }, i) => {
                selectProduct.append(`<option value=${i}>${name}</option>`);
            });
        })
        .catch(e => {
            alert(e.message);
        });
} 

$("#add-product-btn").click(() => {
    productsTable.append(`<tr><td><select data-index="${selectedProductsCount}" id="select-product-${selectedProductsCount}" class="form-select select-product" onchange="setProductDetails(event.target)" required><option value="-1">Select product</option></select></td><td><input data-index="${selectedProductsCount}" id="product-quantity-${selectedProductsCount}" type="number" class="form-control" value="1" onchange="changeQuantity(event.target)" required></td><td><input data-index="${selectedProductsCount}" id="product-price-${selectedProductsCount}" type="number" class="form-control" value="0" onchange="changePrice(event.target)" required></td><td><input data-index="${selectedProductsCount}" id="product-discount-${selectedProductsCount}" type="number" class="form-control" value="0" onchange="changeDiscount(event.target)" required></td><td id="product-amount-${selectedProductsCount}">0</td></tr>`);
    const selectProduct = $(`#select-product-${selectedProductsCount++}`);
    products.map(({ name }, i) => {
        selectProduct.append(`<option value=${i}>${name}</option>`);
    });
    selectedProducts.push({
        id: 0,
        name: "",
        quantity: 1,
        unit: "",
        price: 0,
        discount: 0,
        amount: 0
    });
});

function setProductDetails(target) {
    const productIndex = parseInt(target.value);
    const index = target.getAttribute("data-index");

    if(productIndex === -1) {
        setValues(index, {id: 0, name: "", quantity: 0, price: 0, discount: 0, amount: 0})
        return;
    }

    const selectedProduct = selectedProducts[index];
    const {id, name, unit, price} = products[productIndex]
    selectedProduct.id = id;
    selectedProduct.name = name;
    selectedProduct.unit = unit;
    selectedProduct.price= price;
    selectedProduct.amount = price;

    setValues(index, selectedProduct);
    setSubTotal();
}

function setValues(index, { name, quantity, price, discount, amount }) {
    $(`#product-quantity-${index}`).val(quantity);
    $(`#product-price-${index}`).val(price);
    $(`#product-discount-${index}`).val(discount);
    $(`#product-amount-${index}`).html(price);
}

function changeQuantity(target) {
    const quantity = target.value;
    if (quantity === "") {
        return;
    }
    const index = target.getAttribute("data-index");
    const selectedProduct = selectedProducts[index];
    selectedProduct.quantity = parseFloat(quantity);
    changeAmount(index, selectedProduct);
}

function changeDiscount(target) {
    const discount = target.value;
    if (discount === "") {
        return;
    }
    const index = target.getAttribute("data-index");
    const selectedProduct = selectedProducts[index];
    selectedProduct.discount = parseFloat(discount);
    changeAmount(index, selectedProduct);
}

function changePrice(target) {
    const price = target.value;
    if (price === "") {
        return;
    }
    const index = target.getAttribute("data-index");
    const selectedProduct = selectedProducts[index];
    selectedProduct.price = parseFloat(price);
    changeAmount(index, selectedProduct);
}

function changeAmount(index, selectedProduct) {
    const price = selectedProduct.price;
    const quantity = selectedProduct.quantity;
    const discount = selectedProduct.discount;
    selectedProduct.amount = quantity * (price - discount/100 * price);
    $(`#product-amount-${index}`).html(selectedProduct.amount);
    setSubTotal()
}

function changeAdjustments(target) {
    const adjustment = target.value;
    if (adjustment === "") {
        return;
    }
    total -= adjustments;
    adjustments = parseFloat(adjustment);
    total = total + adjustments;
    $("#total").html(total);
}

function changeShippingCharges(target) {
    const shippingCharge = target.value;
    if (shippingCharge === "") {
        return;
    }
    total -= shippingCharges;
    shippingCharges = parseFloat(shippingCharge);
    total = total + shippingCharges;
    $("#total").html(total);
}

function setSubTotal() {
    total -= subTotal;
    subTotal = 0;
    selectedProducts.map(selectedProduct => {
        subTotal += selectedProduct.amount;
    });
    $("#sub-total").html(subTotal);
    total += subTotal;
    $("#total").html(total);
}

const newInvoiceForm = document.getElementById("new-invoice-form");

newInvoiceForm.addEventListener("submit", e => {
    e.preventDefault();

    const formData = new FormData(newInvoiceForm);
    const requestData = {
        customerId: parseInt(formData.get("customerId")),
        invoiceDate: formData.get("invoiceDate"),
        dueDate: formData.get("dueDate"),
        adjustments: parseFloat(formData.get("adjustments")),
        shippingCharges: parseFloat(formData.get("shippingCharges")),
        notes: formData.get("notes"),
        termsAndConditions: formData.get("termsAndConditions"),
        invoicedProducts: selectedProducts
    }

    axios.post("http://localhost:8080/InNoice/api/invoices", requestData)
        .then(({ data }) => {
            var message;
            if (data == -1) {
                message = "Something went wrong";
            } else if (data == -5) {
                message = "Due date must be after the date of invoice";
            } else if(isNaN(data)) {
                message = "Insufficient data";
            } else {
                message = "Invoice created successfully";
                location.href = "/InNoice/invoices";
            }
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});