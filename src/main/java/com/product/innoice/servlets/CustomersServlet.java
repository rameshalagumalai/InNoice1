package com.product.innoice.servlets;

import com.google.gson.Gson;
import com.product.innoice.db.CustomersDataAccessor;
import com.product.innoice.models.Customer;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

public class CustomersServlet extends HttpServlet {
    @Resource(name = "jdbc/innoice")
    private DataSource dataSource;
    private CustomersDataAccessor customersDataAccessor;
    private Gson gson = new Gson();

    @Override
    public void init() throws ServletException {
        try {
            customersDataAccessor = new CustomersDataAccessor(dataSource);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        final String USER_ID_STRING = req.getParameter("userId"),
                response;
        if (USER_ID_STRING == null) {
            response = new JSONObject().put("message", "Insufficient parameters").toString();
        } else if(!USER_ID_STRING.equals(req.getSession().getAttribute("userId").toString())){
            response = new JSONObject().put("message", "No access").toString();
        } else {
            final int USER_ID = Integer.parseInt(USER_ID_STRING);
            response = gson.toJson(customersDataAccessor.getCustomersByUser(USER_ID));
        }
        PrintWriter writer = resp.getWriter();
        writer.println(response);
        endWriter(writer);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        InputStream inputStream = new BufferedInputStream(req.getInputStream());
        String result = IOUtils.toString(inputStream, "UTF-8");
        JSONObject reqObj = new JSONObject(result);
        inputStream.close();
        addNewCustomer(reqObj, resp);
    }

    private void addNewCustomer(JSONObject reqObj, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();

        final String NAME = reqObj.getString("name"),
                EMAIL = reqObj.getString("email"),
                ADDRESS = reqObj.getString("address"),
                CONTACT = reqObj.getString("contact");
        final Integer USER_ID = reqObj.getInt("userId");

        if (NAME == null || EMAIL == null || ADDRESS == null || CONTACT == null || USER_ID == null) {
            writer.println("Insufficient data");
            return;
        }

        Customer customer = new Customer(USER_ID, NAME, EMAIL, ADDRESS, CONTACT);
        final int RESPONSE = customersDataAccessor.addNewCustomer(customer);

        writer.println(RESPONSE);
        endWriter(writer);
    }

    private void endWriter(PrintWriter writer) {
        writer.flush();
        writer.close();
    }

    @Override
    public void destroy() {
        System.out.println("Customers servlet destroyed");
    }
}
