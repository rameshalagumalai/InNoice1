package com.product.innoice.db;

import org.jasypt.util.text.BasicTextEncryptor;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class SessionsDataAccessor {
    private DataSource dataSource;

    public SessionsDataAccessor(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public int signInUser(final String EMAIL, final String PASSWORD, final String SESSION_ID) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int result = -1;

        try {
            connection = dataSource.getConnection();
            final String SQL = "SELECT id, password FROM user WHERE email = ?";

            preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, EMAIL);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
                textEncryptor.setPasswordCharArray("yuvaraaj".toCharArray());
                final String DB_PASSWORD = textEncryptor.decrypt(resultSet.getString("password"));
                if (PASSWORD.equals(DB_PASSWORD)) {
                    result = resultSet.getInt("id");
                }
            } else {
                result = -2;
            }

            close(connection, preparedStatement, resultSet);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, null);
        }

        return result;
    }

    private void close(Connection connection, Statement statement, ResultSet resultSet) {
        try {
            if (resultSet != null)  {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connection != null) {
                connection.close();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
