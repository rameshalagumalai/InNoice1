package com.product.innoice.servlets;

import com.google.gson.Gson;
import com.product.innoice.db.InvoicesDataAccessor;
import com.product.innoice.db.PaymentsDataAccessor;
import com.product.innoice.models.Customer;
import com.product.innoice.models.Payment;
import com.product.innoice.models.ResponseInvoice;
import com.product.innoice.models.ResponsePayment;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

public class PaymentServlet extends HttpServlet {
    @Resource(name = "jdbc/innoice")
    private DataSource dataSource;
    private PaymentsDataAccessor paymentsDataAccessor;
    private InvoicesDataAccessor invoicesDataAccessor;
    private Gson gson = new Gson();

    @Override
    public void init() throws ServletException {
        try {
            paymentsDataAccessor = new PaymentsDataAccessor(dataSource);
            invoicesDataAccessor = new InvoicesDataAccessor(dataSource);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        PrintWriter writer = resp.getWriter();
        final int ID = Integer.parseInt(req.getPathInfo().split("/")[1]);
        ResponsePayment payment = paymentsDataAccessor.getPaymentById(ID);
        ResponseInvoice invoice = invoicesDataAccessor.getInvoiceById(payment.getInvoiceId());
        writer.println(new JSONObject(paymentsDataAccessor.getPaymentById(ID)).put("invoice", new JSONObject(invoice)));
        endWriter(writer);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        InputStream inputStream = new BufferedInputStream(req.getInputStream());
        String result = IOUtils.toString(inputStream, "UTF-8");
        String id = req.getPathInfo().split("/")[1];
        JSONObject reqObj = new JSONObject(result);
        reqObj.put("id", id);
        inputStream.close();
        editPayment(reqObj, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        final int ID = Integer.parseInt(req.getPathInfo().split("/")[1]);
        final int RESPONSE = paymentsDataAccessor.deletePayment(ID);
        writer.println(RESPONSE);
        endWriter(writer);
    }

    private void editPayment(JSONObject reqObj, HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();

        final Integer ID = reqObj.getInt("id"),
                INVOICE_ID = reqObj.getInt("invoiceId");
        final Double AMOUNT = reqObj.getDouble("amount");
        final String DATE = reqObj.getString("date");

        if (ID == null || INVOICE_ID == null || AMOUNT == null || DATE == null) {
            writer.println("Insufficient data");
            return;
        }

        Payment payment = new Payment(ID, INVOICE_ID, AMOUNT, DATE);
        writer.println(paymentsDataAccessor.editPayment(payment));
        endWriter(writer);
    }

    private void endWriter(PrintWriter writer) {
        writer.flush();
        writer.close();
    }

    @Override
    public void destroy() {
        System.out.println("Customer servlet destroyed");
    }
}
