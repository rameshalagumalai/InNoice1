window.onload = () => {
    const href = location.href;
    const id = href.substring(href.lastIndexOf('/')+1);
    axios.get(`http://localhost:8080/InNoice/api/payments/${id}`)
        .then(({ data }) => {
            displayPayment(data);
        })
        .catch(e => {
            alert(e.message);
        });
}

function displayPayment(
    {  
        id,
        invoiceId,
        invoice,
        amount,
        customerName,
        date
    }
){
    const { companyName , companyAddress, customerAddress, invoiceDate, total, balanceDue } = invoice;
    $("#company-name").text(companyName);
    $("#company-address").text(companyAddress);
    $("#payment-date").text(date);
    $("#payment-id").text(`PAY-${id}`);
    $(".payment-amount").text($("#invoice-amount").text()+amount);
    $("#customer-name").text(customerName);
    $("#customer-address").text(customerAddress);
    $("#invoice-id").text(`INV-${invoiceId}`);
    $("#invoice-date").text(invoiceDate);
    $("#invoice-amount").text($("#invoice-amount").text()+total);
}