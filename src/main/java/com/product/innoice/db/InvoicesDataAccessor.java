package com.product.innoice.db;

import com.product.innoice.models.Invoice;
import com.product.innoice.models.InvoicedProduct;
import com.product.innoice.models.ResponseInvoice;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class InvoicesDataAccessor {
    private DataSource dataSource;

    public InvoicesDataAccessor(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public int addNewInvoice(Invoice invoice) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int response = -1;

        try {
            connection = dataSource.getConnection();

            final int CUSTOMER_ID = invoice.getCustomerId();
            final Double ADJUSTMENTS = invoice.getAdjustments(),
                    SHIPPING_CHARGES = invoice.getShippingCharges();
            final String INVOICE_DATE = invoice.getInvoiceDate(),
                    DUE_DATE = invoice.getDueDate(),
                    NOTES = invoice.getNotes(),
                    TERMS_AND_CONDITIONS = invoice.getTermsAndConditions(),
                    SQL = "INSERT INTO invoice (customer_id, invoice_date, due_date, adjustments, shipping_charges, notes, terms_and_conditions) VALUES (?, ?, ?, ?, ?, ?, ?)";

            if (INVOICE_DATE.compareTo(DUE_DATE) == 1) {
                close(connection, preparedStatement, resultSet);
                return -5;
            }

            preparedStatement = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, CUSTOMER_ID);
            preparedStatement.setString(2, INVOICE_DATE);
            preparedStatement.setString(3, DUE_DATE);
            preparedStatement.setDouble(4, ADJUSTMENTS);
            preparedStatement.setDouble(5, SHIPPING_CHARGES);
            preparedStatement.setString(6, NOTES);
            preparedStatement.setString(7, TERMS_AND_CONDITIONS);

            preparedStatement.execute();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                response = resultSet.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }

        return response;
    }

    public List<ResponseInvoice> getInvoicesByUser(final int USER_ID) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null, resultSet1 = null;
        List<ResponseInvoice> responseInvoices = new ArrayList<>();
        try {
            connection = dataSource.getConnection();
            final String SQL = "SELECT invoice.id, customer.name AS customer_name, customer.address AS customer_address, user.company_name, user.address AS company_address, DATE_FORMAT(invoice.invoice_date, '%b %d, %Y') AS invoice_date, DATE_FORMAT(invoice.due_date, '%b %d, %Y') AS due_date, SUM(invoiced_product.total) AS sub_total, invoice.adjustments, invoice.shipping_charges, invoice.notes, invoice.terms_and_conditions, (SELECT SUM(amount) FROM payment WHERE invoice_id = invoice.id) AS amount_paid FROM invoice INNER JOIN customer ON invoice.customer_id = customer.id INNER JOIN user ON customer.user_id = user.id INNER JOIN invoiced_product ON invoiced_product.invoice_id = invoice.id WHERE user.id = ? GROUP BY invoice.id ORDER BY invoice.invoice_date",
                    SQL1 = "SELECT * FROM invoiced_product WHERE invoice_id = ?";
            preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setInt(1, USER_ID);

            int id;
            double adjustments, shippingCharges, subTotal, total, amountPaid, balanceDue;
            String customerName, customerAddress, companyName, companyAddress, invoiceDate, dueDate, notes, termsAndConditions;
            List<InvoicedProduct> invoicedProducts = new ArrayList<>();
            resultSet = preparedStatement.executeQuery();
            preparedStatement = connection.prepareStatement(SQL1);
            while (resultSet.next()) {
                id = resultSet.getInt("id");
                adjustments = resultSet.getDouble("adjustments");
                shippingCharges = resultSet.getDouble("shipping_charges");
                subTotal = resultSet.getDouble("sub_total");
                total = subTotal + adjustments + shippingCharges;
                amountPaid = resultSet.getDouble("amount_paid");
                balanceDue = total - amountPaid;
                customerName = resultSet.getString("customer_name");
                customerAddress = resultSet.getString("customer_address");
                companyName = resultSet.getString("company_name");
                companyAddress = resultSet.getString("company_address");
                invoiceDate = resultSet.getString("invoice_date");
                dueDate = resultSet.getString("due_date");
                notes = resultSet.getString("notes");
                termsAndConditions = resultSet.getString("terms_and_conditions");

                preparedStatement.setInt(1, id);
                resultSet1 = preparedStatement.executeQuery();
                int ipId, invoiceId, productId;
                double quantity, price, discount, ipTotal;
                String name, unit;
                while (resultSet1.next()) {
                    ipId = resultSet1.getInt("id");
                    invoiceId = resultSet1.getInt("invoice_id");
                    productId = resultSet1.getInt("product_id");
                    quantity = resultSet1.getInt("quantity");
                    price = resultSet1.getInt("price");
                    discount = resultSet1.getInt("discount");
                    ipTotal = resultSet1.getInt("total");
                    name = resultSet1.getString("name");
                    unit = resultSet1.getString("unit");
                    invoicedProducts.add(new InvoicedProduct(ipId, invoiceId, productId, quantity, price, discount, ipTotal, name, unit));
                }
                responseInvoices.add(new ResponseInvoice(id, adjustments, shippingCharges, subTotal, total, amountPaid, balanceDue, customerName, customerAddress, companyName,companyAddress, invoiceDate, dueDate, notes, termsAndConditions, invoicedProducts));
                invoicedProducts = new ArrayList<>();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return responseInvoices;
    }

    public ResponseInvoice getInvoiceById(final int ID) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ResponseInvoice responseInvoice = null;
        try {
            connection = dataSource.getConnection();
            final String SQL = "SELECT invoice.id, customer.name AS customer_name, customer.address AS customer_address, user.company_name, user.address AS company_address, DATE_FORMAT(invoice.invoice_date, '%b %d, %Y') AS invoice_date, DATE_FORMAT(invoice.due_date, '%b %d, %Y') AS due_date, SUM(invoiced_product.total) AS sub_total, invoice.adjustments, invoice.shipping_charges, invoice.notes, invoice.terms_and_conditions, (SELECT SUM(amount) FROM payment WHERE invoice_id = invoice.id) AS amount_paid FROM invoice INNER JOIN customer ON invoice.customer_id = customer.id INNER JOIN user ON customer.user_id = user.id INNER JOIN invoiced_product ON invoiced_product.invoice_id = invoice.id WHERE invoice.id = ?",
                    SQL1 = "SELECT * FROM invoiced_product WHERE invoice_id = ?";
            preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setInt(1, ID);

            final int INVOICE_ID;
            final double ADJUSTMENTS, SHIPPING_CHARGES, SUB_TOTAL, TOTAL, AMOUNT_PAID, BALANCE_DUE;
            final String CUSTOMER_NAME, CUSTOMER_ADDRESS, COMPANY_NAME, COMPANY_ADDRESS, INVOICE_DATE, DUE_DATE, NOTES, TERMS_AND_CONDITIONS;
            final List<InvoicedProduct> INVOICED_PRODUCTS = new ArrayList<>();
            resultSet = preparedStatement.executeQuery();
            preparedStatement = connection.prepareStatement(SQL1);
            if (resultSet.next()) {
                INVOICE_ID = resultSet.getInt("id");
                ADJUSTMENTS = resultSet.getDouble("adjustments");
                SHIPPING_CHARGES = resultSet.getDouble("shipping_charges");
                SUB_TOTAL = resultSet.getDouble("sub_total");
                TOTAL = SUB_TOTAL + ADJUSTMENTS + SHIPPING_CHARGES;
                AMOUNT_PAID = resultSet.getDouble("amount_paid");
                BALANCE_DUE = TOTAL - AMOUNT_PAID;
                CUSTOMER_NAME = resultSet.getString("customer_name");
                CUSTOMER_ADDRESS = resultSet.getString("customer_address");
                COMPANY_NAME = resultSet.getString("company_name");
                COMPANY_ADDRESS = resultSet.getString("company_address");
                INVOICE_DATE = resultSet.getString("invoice_date");
                DUE_DATE = resultSet.getString("due_date");
                NOTES = resultSet.getString("notes");
                TERMS_AND_CONDITIONS = resultSet.getString("terms_and_conditions");

                preparedStatement.setInt(1, INVOICE_ID);
                resultSet = preparedStatement.executeQuery();
                int ipId, invoiceId, productId;
                double quantity, price, discount, ipTotal;
                String name, unit;
                while (resultSet.next()) {
                    ipId = resultSet.getInt("id");
                    invoiceId = resultSet.getInt("invoice_id");
                    productId = resultSet.getInt("product_id");
                    quantity = resultSet.getInt("quantity");
                    price = resultSet.getInt("price");
                    discount = resultSet.getInt("discount");
                    ipTotal = resultSet.getInt("total");
                    name = resultSet.getString("name");
                    unit = resultSet.getString("unit");
                    INVOICED_PRODUCTS.add(new InvoicedProduct(ipId, invoiceId, productId, quantity, price, discount, ipTotal, name, unit));
                }
                responseInvoice = new ResponseInvoice(INVOICE_ID, ADJUSTMENTS, SHIPPING_CHARGES, SUB_TOTAL, TOTAL, AMOUNT_PAID, BALANCE_DUE, CUSTOMER_NAME, CUSTOMER_ADDRESS, COMPANY_NAME,COMPANY_ADDRESS, INVOICE_DATE, DUE_DATE, NOTES, TERMS_AND_CONDITIONS, INVOICED_PRODUCTS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return responseInvoice;
    }

    public int deleteInvoice(final int ID) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int result = -1;
        try{
            connection = dataSource.getConnection();

            final String SQL = "DELETE FROM invoice WHERE id = ?";

            preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setInt(1, ID);

            result = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, null);
        }

        return result;
    }

    private void close(Connection connection, Statement statement, ResultSet resultSet) {
        try {
            if (resultSet != null)  {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connection != null) {
                connection.close();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
