package com.product.innoice.filters;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class ApiFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        HttpSession session = req.getSession();

        final Integer USER_ID = (Integer) session.getAttribute("userId");

        PrintWriter writer = res.getWriter();

        if (USER_ID == null) {
            writer.println("Better luck next time");
            endWriter(writer);
        } else {
            chain.doFilter(request, response);
        }
    }

    private void endWriter(PrintWriter writer) {
        writer.close();
        writer.flush();
    }

    @Override
    public void destroy() {

    }
}
