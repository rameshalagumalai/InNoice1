var invoices = [];
const userId = $("#user-id").val();

$("#new-invoice-page-btn").click(() => {
    location.href = "/InNoice/invoices/new";
});

window.onload = () => {
    axios.get(`http://localhost:8080/InNoice/api/invoices?userId=${userId}`)
        .then(({ data }) => {
            invoices = data;
            const customersTable = $("#invoices-table");
            var statusTd;
            invoices.map(({ invoiceDate, id, customerName, dueDate, total, amountPaid, balanceDue }, i) => {
                var disabled = "";
                var deleteDisabled = "";
                if (balanceDue === 0) {
                    statusTd = `<td class='text-success' onclick='redirectToInvoicePage(${id})'>PAID</td>`;
                    disabled = " disabled";
                    deleteDisabled = " disabled";
                } else if (balanceDue < total) {
                    statusTd = `<td class='text-warning' onclick='redirectToInvoicePage(${id})'>PARTIALLY PAID</td>`;
                    deleteDisabled = " disabled";
                } else {
                    statusTd = `<td class='text-secondary' onclick='redirectToInvoicePage(${id})'>SENT</td>`;
                }
                customersTable.append(`<tr><td onclick="redirectToInvoicePage(${id})">${invoiceDate}</td><td onclick="redirectToInvoicePage(${id})">INV-${id}</td><td onclick="redirectToInvoicePage(${id})">${customerName}</td>${statusTd}<td onclick="redirectToInvoicePage(${id})">${dueDate}</td><td onclick="redirectToInvoicePage(${id})">&#8377;${total}</td><td onclick="redirectToInvoicePage(${id})">&#8377;${balanceDue}</td><td><div class="dropdown"><button class="btn btn-outline-secondary btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">Select</button><ul class="dropdown-menu"><li class="dropdown-item cursor-pointer${disabled}" onclick="setRecordPaymentModalContent(${i})" data-bs-toggle="modal" data-bs-target="#exampleModal8">Record payment</li><li class="dropdown-item cursor-pointer${deleteDisabled}" onclick="$('#delete-id').val(${id})" data-bs-toggle="modal" data-bs-target="#exampleModal7">Delete</a></li></ul></div></td></tr>`);
            });
        })
        .catch(e => {
            alert(e.message);
        });
}

function redirectToInvoicePage(id) {
    location.href = `/InNoice/invoices/${id}`;
}

function setRecordPaymentModalContent(i) {
    const { id, customerName, invoiceDate, dueDate, total, amountPaid, balanceDue } = invoices[i];
    $("#invoice-id").text(`INV-${id}`);
    $("#invoice-customer").text(customerName);
    $("#invoice-date").text(invoiceDate);
    $("#due-date").text(dueDate);
    $("#invoice-amount").text(total);
    $("#invoice-due").text(balanceDue);
    console.log(id);
    $("#paid-invoice-id").val(id);
}

const recordPaymentForm = document.getElementById("record-payment-form");

recordPaymentForm.addEventListener("submit", e => {
    e.preventDefault();

    const formData = new FormData(recordPaymentForm);
    const requestData = {
        invoiceId: formData.get("invoice-id"),
        date: formData.get("payment-date"),
        amount: formData.get("payment-amount")
    }

    axios.post("http://localhost:8080/InNoice/api/payments", requestData)
        .then(({ data }) => {
            var message;
            switch (data) {
                case 1:
                    message = "Payment recorded successfully";
                    location.href = "/InNoice/payments-received";
                    break;
                case -2:
                    message = "Payment exceeds the balance due";
                    break;
                case -3:
                    message = "Invalid payment amount";
                    break;
                case -4:
                    message = "Payment must be done after the issue of the invoice";
                    break;     
                default:
                    message = "Something went wrong";
            }
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});

const deleteInvoiceForm = document.getElementById("delete-invoice-form");

deleteInvoiceForm.addEventListener("submit", e => {
    e.preventDefault();

    const formData = new FormData(deleteInvoiceForm);

    axios.delete(`http://localhost:8080/InNoice/api/invoices/${formData.get('id')}`)
        .then(({ data }) => {
            var message;
            if (data === 1) {
                message = "Invoice deleted successfully";
                location.reload();
            } else {
                message = "Something went wrong";
            }   
            alert(message);
        })
        .catch(e => {
            alert(e.message);
        });
});