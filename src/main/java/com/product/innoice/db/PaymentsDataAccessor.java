package com.product.innoice.db;

import com.product.innoice.models.Payment;
import com.product.innoice.models.ResponsePayment;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PaymentsDataAccessor {
    private DataSource dataSource;

    public PaymentsDataAccessor(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public int addNewPayment(Payment payment) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int response = -1;
        try {
            connection = dataSource.getConnection();

            final Integer INVOICE_ID = payment.getInvoiceId();
            final Double AMOUNT = payment.getAmount();
            final String DATE = payment.getDate(),
                    SQL = "INSERT INTO payment (date, amount, invoice_id) VALUES (?, ?, ?)";

            if (AMOUNT <= 0) {
                connection.close();
                return -3;
            }

            if (!checkDate(connection, DATE, INVOICE_ID)) {
                connection.close();
                return -4;
            }

            if(!checkAmountRemaining(connection, AMOUNT, INVOICE_ID, "")) {
                connection.close();
                return -2;
            }

            preparedStatement = connection.prepareStatement(SQL);

            preparedStatement.setString(1, DATE);
            preparedStatement.setDouble(2, AMOUNT);
            preparedStatement.setInt(3, INVOICE_ID);

            response = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, null);
        }
        return response;
    }

    public List<ResponsePayment> getPaymentsByUser(final int USER_ID) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<ResponsePayment> responsePayments = new ArrayList<>();

        try {
            connection = dataSource.getConnection();

            final String SQL = "SELECT payment.id, DATE_FORMAT(payment.date, '%b %d, %Y') AS date, payment.invoice_id, customer.name AS customer_name, payment.amount FROM payment INNER JOIN invoice ON payment.invoice_id = invoice.id INNER JOIN customer ON invoice.customer_id = customer.id WHERE customer.user_id = ?";
            preparedStatement = connection.prepareStatement(SQL);

            preparedStatement.setInt(1, USER_ID);
            resultSet = preparedStatement.executeQuery();

            int id, invoiceId;
            double amount;
            String customerName, date;
            while (resultSet.next()) {
                id = resultSet.getInt("id");
                invoiceId = resultSet.getInt("invoice_id");
                amount = resultSet.getDouble("amount");
                customerName = resultSet.getString("customer_name");
                date = resultSet.getString("date");
                responsePayments.add(new ResponsePayment(id, invoiceId, amount, customerName, date));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }

        return responsePayments;
    }

    public ResponsePayment getPaymentById(final int ID) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ResponsePayment responsePayment = null;

        try {
            connection = dataSource.getConnection();

            final String SQL = "SELECT payment.id, DATE_FORMAT(payment.date, '%b %d, %Y') AS date, payment.invoice_id, customer.name AS customer_name, payment.amount FROM payment INNER JOIN invoice ON payment.invoice_id = invoice.id INNER JOIN customer ON invoice.customer_id = customer.id WHERE payment.id = ?";
            preparedStatement = connection.prepareStatement(SQL);

            preparedStatement.setInt(1, ID);
            resultSet = preparedStatement.executeQuery();

            final int PAYMENT_ID, INVOICE_ID;
            double AMOUNT;
            String CUSTOMER_NAME, DATE;
            if (resultSet.next()) {
                PAYMENT_ID = resultSet.getInt("id");
                INVOICE_ID = resultSet.getInt("invoice_id");
                AMOUNT = resultSet.getDouble("amount");
                CUSTOMER_NAME = resultSet.getString("customer_name");
                DATE = resultSet.getString("date");
                responsePayment = new ResponsePayment(PAYMENT_ID, INVOICE_ID, AMOUNT, CUSTOMER_NAME, DATE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, resultSet);
        }

        return responsePayment;
    }

    public int editPayment(Payment payment) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int response = -1;
        try{
            connection = dataSource.getConnection();

            final int ID = payment.getId(),
                    INVOICE_ID = payment.getInvoiceId();
            final double AMOUNT = payment.getAmount();
            final String DATE = payment.getDate(),
                    EXTRA = " - (SELECT amount FROM payment WHERE id = "+ ID +")";

            if (AMOUNT <= 0) {
                connection.close();
                return -3;
            }

            if (!checkDate(connection, DATE, INVOICE_ID)) {
                connection.close();
                return -4;
            }

            if(!checkAmountRemaining(connection, AMOUNT, INVOICE_ID, EXTRA)) {
                connection.close();
                return -2;
            }

            final String SQL = "UPDATE payment SET date = ?, amount = ? WHERE id = ?";
            preparedStatement = connection.prepareStatement(SQL);

            preparedStatement.setString(1, DATE);
            preparedStatement.setDouble(2, AMOUNT);
            preparedStatement.setInt(3, ID);

            response = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, null);
        }

        return response;
    }

    public int deletePayment(final int ID) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int result = -1;
        try{
            connection = dataSource.getConnection();

            final String SQL = "DELETE FROM payment WHERE id = ?";

            preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setInt(1, ID);

            result = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(connection, preparedStatement, null);
        }

        return result;
    }

    private boolean checkAmountRemaining(Connection connection, final double AMOUNT, final int INVOICE_ID, final String EXTRA) throws SQLException {
        final String SQL = "SELECT (SUM(invoiced_product.total) + invoice.adjustments + invoice.shipping_charges) AS total, ((SELECT sum(amount) FROM payment WHERE invoice_id = invoice.id)" + EXTRA + ") AS paid_amount FROM invoice INNER JOIN invoiced_product ON invoiced_product.invoice_id = invoice.id  WHERE invoice.id = ? GROUP BY invoice.id";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL);
        ResultSet resultSet = null;
        final double TOTAL, PAID_AMOUNT;

        preparedStatement.setInt(1, INVOICE_ID);
        resultSet = preparedStatement.executeQuery();

        if (resultSet.next()) {
            TOTAL = resultSet.getDouble("total");
            PAID_AMOUNT = resultSet.getDouble("paid_amount");
            if (TOTAL - PAID_AMOUNT >= AMOUNT) {
                close(null, preparedStatement, resultSet);
                return true;
            }
        }
        close(null, preparedStatement, resultSet);
        return false;
    }

    private boolean checkDate(Connection connection, final String DATE, final int INVOICE_ID) throws SQLException {
        final String SQL = "SELECT (invoice_date <= ? AND due_date >= ?) AS ok FROM invoice WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(SQL);
        ResultSet resultSet = null;
        boolean response = false;

        preparedStatement.setString(1, DATE);
        preparedStatement.setString(2, DATE);
        preparedStatement.setInt(3, INVOICE_ID);
        resultSet = preparedStatement.executeQuery();

        if (resultSet.next()) {
            response = resultSet.getBoolean(1);
            close(null, preparedStatement, resultSet);
        }
        close(null, preparedStatement, resultSet);
        return response;
    }

    private void close(Connection connection, Statement statement, ResultSet resultSet) {
        try {
            if (resultSet != null)  {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connection != null) {
                connection.close();
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
