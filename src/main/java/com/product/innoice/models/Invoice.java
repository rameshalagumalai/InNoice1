package com.product.innoice.models;

import java.util.List;

public class Invoice {
    private int id, customerId;
    private double adjustments, shippingCharges;
    private String invoiceDate, dueDate, notes, termsAndConditions;

    public Invoice(int customerId, double adjustments, double shippingCharges, String invoiceDate, String dueDate, String notes, String termsAndConditions) {
        this.customerId = customerId;
        this.adjustments = adjustments;
        this.shippingCharges = shippingCharges;
        this.invoiceDate = invoiceDate;
        this.dueDate = dueDate;
        this.notes = notes;
        this.termsAndConditions = termsAndConditions;
    }

    public Invoice(int id, int customerId, double adjustments, double shippingCharges, String invoiceDate, String dueDate, String notes, String termsAndConditions) {
        this.id = id;
        this.customerId = customerId;
        this.adjustments = adjustments;
        this.shippingCharges = shippingCharges;
        this.invoiceDate = invoiceDate;
        this.dueDate = dueDate;
        this.notes = notes;
        this.termsAndConditions = termsAndConditions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public double getAdjustments() {
        return adjustments;
    }

    public void setAdjustments(double adjustments) {
        this.adjustments = adjustments;
    }

    public double getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(double shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(String termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }
}
